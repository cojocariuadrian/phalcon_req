<?php

class IndexController extends ControllerBase
{

    public function indexAction()
    {
		$this->view->disable();
		
		$response = new Phalcon\Http\Response();
		$response->setContentType('text/plain');
		$response->setStatusCode(200, 'OK');
		
		$content = <<<XML
<transactions>
	<trans timestamp="2013-02-27 00:03:01" uni="d3fe23585654ae32f0954271e0538091" typ="abo_start" ref="123451" prov="3.49" />
	<trans timestamp="2013-02-27 00:03:02" uni="d3fe23585654ae32f0954271e0538092" typ="abo_rebill" ref="123452" prov="4.49" />
	<trans timestamp="2013-02-27 00:03:03" uni="d3fe23585654ae32f0954271e0538093" typ="single" ref="123453" prov="5.49" />


</transactions>
XML;
		
		$response->setContent($content);
		$response->send();
    }

}

