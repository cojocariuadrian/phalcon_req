set :branch, 'development'
set :deploy_to, "/var/www/vhosts/#{application}"
server 'dev-delivery.brokerbabe.com', :app, :web, :primary => true