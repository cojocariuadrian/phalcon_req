require 'capistrano/ext/multistage'

set :application, "req-test"
set :repository,  "git@bitbucket.org:cojocariuadrian/phalcon_req.git"

set :scm, :git

set :stages, ["develop", "production"]
set :default_stage, "develop"

set :use_sudo, false

set :ssh_options, { :forward_agent => true, :keys => '~/.ssh/id_rsa'}
#default_run_options[:pty] = true

set :user, "deployment"

#after "deploy:restart", "deploy:cleanup"

#after "deploy:restart", "deploy:composer"

#namespace :deploy do
#	task :composer, :roles => :app do
#        run "cd #{current_path} && sudo curl -sS https://getcomposer.org/installer | php"
#        run "cd #{current_path} && sudo php composer.phar install"
#	end
#end